/*
 * _74HC595.h
 *
 * Created: 10/7/2022
 *  Author: Guyrandy Jean-Gilles
 */ 
#include <stdint.h>
#define DATA_TYPE uint16_t  // probably should figure out how to remove this from the header...

#ifndef FIXME_MACRO_H_
#define FIXME_MACRO_H_


void setRegister(uint8_t inputPin, uint8_t clockPin, uint8_t latch_pin, DATA_TYPE data);


#endif /* 74HC595_H_ */