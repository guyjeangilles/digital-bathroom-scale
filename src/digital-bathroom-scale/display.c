/*
 * display.c
 *
 * Created: 10/10/2022
 *  Author: Guyrandy Jean-Gilles
 */ 
#include "display.h"
#include "tdcx1050m.h"

bool displayError(void)
{
	bool displayFailure = false;
	displayFailure |= setSegment(1, G);
	displayFailure |= setSegment(2, G);
	displayFailure |= setSegment(3, G);
	displayFailure |= setSegment(4, G);

	return displayFailure;
}

/* Displays a three digit number with one decimal point precision
* Must be continually called
* No blocking code can be in it's loop
*/
bool displayNumber(float number)
{
	if (number < 0 || number > 999.9) // don't support negative numbers
	{
		displayError();
		return 1;
	}
	uint8_t hundredsDigit, tensDigit, onesDigit, thousanthDigit;
	
	int number_int = (int) number;
	hundredsDigit = number_int / 100;
	number_int -= hundredsDigit * 100;

	tensDigit = number_int / 10;
	number_int -= tensDigit * 10;

	onesDigit = number_int;
	thousanthDigit = (10 * (number  - (hundredsDigit * 100) - (tensDigit * 10) - onesDigit)) / 1;

	bool displayFailure = false;
	displayFailure |= setDigit(1, hundredsDigit);
	displayFailure |= setDigit(2, tensDigit);
	displayFailure |= setDigit(3, onesDigit);
	displayFailure |= setDecimalPoint(3);
	displayFailure |= setDigit(4, thousanthDigit);
	
	if (displayFailure)
	{
		displayError();
		return 1;
	}
	return 0;
}