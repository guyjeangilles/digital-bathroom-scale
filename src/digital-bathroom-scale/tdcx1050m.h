/*
 * tdcx1050m.h
 *
 * This library assumes the display is connected to shift registers.
 *
 * Created: 10/8/2022
 *  Author: Guyrandy Jean-Gilles
 */
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	A, B, C, D, E, F, G
} segment_t; // digit could probably also be an enum

bool setDigit(uint8_t digit, uint8_t value);

bool setDecimalPoint(uint8_t digit);

bool setSegment(uint8_t digit, segment_t displaySegment);

void setSemicolon(bool value);

void setL3(bool value);