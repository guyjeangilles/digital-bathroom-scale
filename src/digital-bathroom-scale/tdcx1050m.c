/*
 * tdcx1050m.c
 *
 * Created: 10/9/2022
 *  Author: Guyrandy Jean-Gilles
 */ 
#include "gpio.h"
#include "74HC595.h"
#include "tdcx1050m.h"

#define DIGIT_1_ANODE_BIT 0
#define DIGIT_2_ANODE_BIT 1
#define DIGIT_3_ANODE_BIT 5
#define DIGIT_4_ANODE_BIT 7
#define L1_L2_ANODE_BIT 3
#define L3_ANODE_BIT 9
#define A_SEGMENT_BIT 13
#define B_SEGMENT_BIT 15
#define C_SEGMENT_BIT 12
#define D_SEGMENT_BIT 2
#define E_SEGMENT_BIT 4
#define F_SEGMENT_BIT 10
#define G_SEGMENT_BIT 14
#define DP_SEGMENT_BIT 6
#define L1_L2_SEGMENT_BIT 11
#define L3_SEGMENT_BIT 8

bool isValidDigit(uint8_t digit)
{
	return digit >= 1 && digit <= 4;
}

bool isValidDigitBit(uint16_t digitBit)
{
	return digitBit <= 15;
}

uint16_t getEmptyDisplay(){
	uint16_t output = 0;
	
	// pull all anodes low
	output &= ~((1UL) << (DIGIT_1_ANODE_BIT));
	output &= ~((1UL) << (DIGIT_2_ANODE_BIT));
	output &= ~((1UL) << (DIGIT_3_ANODE_BIT));
	output &= ~((1UL) << (DIGIT_4_ANODE_BIT));
	output &= ~((1UL) << (L1_L2_ANODE_BIT));
	output &= ~((1UL) << (L3_ANODE_BIT));
	
	// pull all cathods high
	output |= (1UL) << (A_SEGMENT_BIT);
	output |= (1UL) << (B_SEGMENT_BIT);
	output |= (1UL) << (C_SEGMENT_BIT);
	output |= (1UL) << (D_SEGMENT_BIT);
	output |= (1UL) << (E_SEGMENT_BIT);
	output |= (1UL) << (F_SEGMENT_BIT);
	output |= (1UL) << (G_SEGMENT_BIT);
	output |= (1UL) << (DP_SEGMENT_BIT);
	return output;
}

uint16_t getDigitBit(uint8_t digit)
{
	uint16_t bit = 16;
	if (digit == 1)
	{
		bit = DIGIT_1_ANODE_BIT;
	} else if (digit == 2)
	{
		bit = DIGIT_2_ANODE_BIT;
	} else if (digit == 3)
	{
		bit = DIGIT_3_ANODE_BIT;
	} else if (digit == 4)
	{
		bit = DIGIT_4_ANODE_BIT;
	}
	return bit;
}

bool setDigit(uint8_t digit, uint8_t value)
{
	if ( !isValidDigit(digit) || value > 9)
	{
		return 1;
	}
	
	uint16_t digitBit = getDigitBit(digit);
	if (!isValidDigitBit(digitBit))
	{
		return 1;
	}
	
	uint16_t display_output = getEmptyDisplay();
	display_output |= 1UL << digitBit;
	
	if (value == 0)
	{
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
		display_output &= ~((1UL) << (E_SEGMENT_BIT));
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
	} else if (value == 1)	
	{						
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
	} else if (value == 2)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
		display_output &= ~((1UL) << (E_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
	} else if (value == 3)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
	} else if (value == 4)	
	{						
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
	} else if (value == 5)			
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
	} else if (value == 6)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
		display_output &= ~((1UL) << (E_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
	} else if (value == 7)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
	} else if (value == 8)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
		display_output &= ~((1UL) << (E_SEGMENT_BIT));
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
	} else if (value == 9)	
	{						
		display_output &= ~((1UL) << (A_SEGMENT_BIT));
		display_output &= ~((1UL) << (B_SEGMENT_BIT));
		display_output &= ~((1UL) << (C_SEGMENT_BIT));
		display_output &= ~((1UL) << (D_SEGMENT_BIT));
		display_output &= ~((1UL) << (F_SEGMENT_BIT));
		display_output &= ~((1UL) << (G_SEGMENT_BIT));
	}
	
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, display_output);
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, getEmptyDisplay());

	return 0;
}

bool setDecimalPoint(uint8_t digit)
{
	if (!isValidDigit(digit))
	{
		return 1;
	}

	uint16_t digitBit = getDigitBit(digit);
	if (!isValidDigitBit(digitBit))
	{
		return 1;
	}

	uint16_t displayOutput = getEmptyDisplay();
	displayOutput |= 1UL << digitBit;
	displayOutput &= ~((1UL) << (DP_SEGMENT_BIT));
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, displayOutput);
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, getEmptyDisplay());
	return 0;
}

bool setSegment(uint8_t digit, segment_t displaySegment)
{
	if (!isValidDigit(digit))
	{
		return 1;
	}

	uint16_t digitBit = getDigitBit(digit);
	if (!isValidDigitBit(digitBit))
	{
		return 1;
	}
	
	uint16_t displayOutput = getEmptyDisplay();
	displayOutput |= 1UL << digitBit;
	
	if (displaySegment == A)
	{
		displayOutput &= ~((1UL) << (A_SEGMENT_BIT));
	} else if (displaySegment == B)
	{
		displayOutput &= ~((1UL) << (B_SEGMENT_BIT));
	} else if (displaySegment == C)
	{
		displayOutput &= ~((1UL) << (C_SEGMENT_BIT));
	} else if (displaySegment == D)
	{
		displayOutput &= ~((1UL) << (D_SEGMENT_BIT));
	} else if (displaySegment == E)
	{
		displayOutput &= ~((1UL) << (E_SEGMENT_BIT));
	}
	else if (displaySegment == F)
	{
		displayOutput &= ~((1UL) << (F_SEGMENT_BIT));
	}
	else if (displaySegment == G)
	{
		displayOutput &= ~((1UL) << (G_SEGMENT_BIT));
	}
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, displayOutput);
	setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, getEmptyDisplay());
	return 0;
}

void setSemicolon(bool value)
{
	if (value)
	{
		uint16_t output = getEmptyDisplay();
		output |= 1UL << L1_L2_ANODE_BIT;
		output &= ~(1UL << L1_L2_SEGMENT_BIT);
		setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, output);
	}
}

void setL3(bool value)
{
	if (value)
	{
		uint16_t output = getEmptyDisplay();
		output |= 1UL << L3_ANODE_BIT;
		output &= ~(1UL << L3_SEGMENT_BIT);
		setRegister(REGISTER_INPUT_PIN, REGISTER_CLOCK_PIN, REGISTER_LATCH_PIN, output);
	}
}