#include "gpio.h"
#include "adc.h"
#include "display.h"

#define STATUS_LED_PIN PA04

int main(void)
{
	gpio_init();
	adcInit();

	uint8_t adc_buffer[2];
	float voltage = 0;
	float conversion_factor = 133.3; // pounds
	float load = 0;
	
	gpio_write(STATUS_LED_PIN, true);
	while (1) {
		adcRead(adc_buffer);
		voltage = (adc_buffer[0] / 4096.) * 3.3;
		load = voltage * conversion_factor;
		if (load > 50.0)
		{
			displayNumber(load);
			gpio_write(STATUS_LED_PIN, false);
		} else {
			gpio_write(STATUS_LED_PIN, true);
		}
	}
}
