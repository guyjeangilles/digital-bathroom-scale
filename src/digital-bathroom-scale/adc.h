/*
 * adc.h
 *
 * Created: 12/3/2022 8:49:28 PM
 *  Author: Blue Mountain
 */ 
#include "gpio.h"

#ifndef ADC_H_
#define ADC_H_

void adcInit(void);
void adcRead(uint8_t *buffer);



#endif /* ADC_H_ */