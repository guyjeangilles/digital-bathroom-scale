/*
 * _74HC595.c
 *
 * Created: 10/7/2022
 *  Author: Guyrandy Jean-Gilles
 */ 

#include "gpio.h"
#include "74HC595.h"

#define NUM_REGISTERS 2
#define DATA_SIZE 8 * NUM_REGISTERS

void shiftBits(uint8_t inputPin, uint8_t clockPin, DATA_TYPE data)
{
	for (DATA_TYPE i = 0; i < DATA_SIZE; i++)
	{
		gpio_write(inputPin, !!(data & (data & (1 << i))));
		gpio_write(clockPin, 1);
		gpio_write(clockPin, 0);
	}
}

void setRegister(uint8_t inputPin, uint8_t clockPin, uint8_t latch_pin, DATA_TYPE data)
{
	gpio_write(latch_pin, 0);
	shiftBits(inputPin, clockPin, data);
	gpio_write(latch_pin, 1);
}