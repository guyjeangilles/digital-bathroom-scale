/*
 * display.h
 *
 * Created: 10/10/2022
 *  Author: Guyrandy Jean-Gilles
 */ 
#include <stdbool.h>

#ifndef DISPLAY_H_
#define DISPLAY_H_


bool displayNumber(float number);


#endif /* DISPLAY_H_ */