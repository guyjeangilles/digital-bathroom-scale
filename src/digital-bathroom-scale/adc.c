/*
 * adc.c
 *
 * Created: 12/3/2022 8:53:54 PM
 *  Author: Guyrandy Jean-Gilles
 */ 
#include "gpio.h"

void adcInit(void)
{
	adc_sync_enable_channel(&ADC_0, 0);
}

void adcRead(uint8_t *buffer)
{
	adc_sync_read_channel(&ADC_0, 0, buffer, 2);
}