/*
 * gpio.h
 *
 * Created: 10/7/2022
 *  Author: Guyrandy Jean-Gilles
 */ 
#include <atmel_start.h>

#ifndef GPIO_H_
#define GPIO_H_

#define REGISTER_INPUT_PIN PA05
#define REGISTER_CLOCK_PIN PA08
#define REGISTER_LATCH_PIN PA09

inline void gpio_init()
{
	atmel_start_init();
}

inline void gpio_write( uint8_t const pin, bool const level)
{
	gpio_set_pin_level(pin, level);
}



#endif /* GPIO_H_ */