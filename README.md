# Digital Bathroom Scale

An open source digital scale for up to 440 pounds (200 kilograms).

This project uses the [ATSAMD11C14](https://www.microchip.com/en-us/product/ATSAMD11C14) and [Atmel Start Framework 4](https://microchipdeveloper.com/atstart:start) from Microchip. The software is written such that any microcontroller can be used. Modify `gpio.h/c` in the source directory to port to your platform.

![schematic](media/schematic.jpg)
![bare pcb](media/populated_pcb.jpg)

## Why?
Search "load cell" or "load sensor" and you'll find a plethora of implementations to study. Almost all use the [HX711](https://www.lcsc.com/product-detail/Analog-To-Digital-Converters-ADCs_Avia-Semicon-Xiamen-HX711_C43656.html); which is a fine chip, but it's mostly useful because of its a built-in gain. An operational amplifier can work just as well and, at quantity, can be cheaper.

## Theory

A load sensor is a [voltage divider](https://en.wikipedia.org/wiki/Voltage_divider), where one resistor is variable and changes with weight on the sensor.

![variable resistor voltage divider](media/3wirestraingauge.png)

This project combines four load sensors into a [wheatstone bridge](https://en.wikipedia.org/wiki/Wheatstone_bridge) such that the voltage change across the sensors sum. `Vb` in the diagram below, is the input to an op amp in a [differential amplifier configuration](https://en.wikipedia.org/wiki/Differential_amplifier#Operational_amplifier_as_differential_amplifier) with 100 gain.

![wheatstone bridge](media/wheatstone_bridge.gif)

## Asssembly

### Mechanical

The `STLs` directory contains 3D printable files for the load sensor mounts. The following images show how to assemble them.

![load sensor mount](media/sensor_mount.jpg)
![half assembled load sensor mount](media/sensor_mount_half_assembled.jpg)
![assembled load sensor mount](media/sensor_mount_assembled.jpg)

I used #8 self-tapping screws to mount four load sensors to the corners of a 12"x12"x0.75" composite. You can use whatever platform or fasteners you want. "Self-tapping screws" might be called "sheet metal screws" at your hardware store. Some tape to manage wires is useful as well.

![mounted sensors](media/mounted_sensors.jpg)

### Electrical

Use the schematic and PCB silkscreen to solder the components to the board. I suggest soldering the ATSAMD11 first as it's the only SMD part and can be difficult to hand-solder with surrounding components in place. Note: All resistors are 1k except the feedback resistors R11 and R12.

![bare pcb](media/bare_pcb.jpg)

From left to right, the 1x3 screw terminals accept the load sensors positive, output, and negative wires. For [Sparkfun's load sensor (product 10245)](https://www.sparkfun.com/products/10245), positive is white, output is red, and negative is black. **YOUR LOAD SENSORS WIRES MAY BE DIFFERENT.**

![load sensor screw terminals](media/sensor_screw_terminals.jpg)

From left to right, the 1x2 screw terminal accepts the positive and negative terminal.

![battery screw terminals](media/battery_screw_terminals.jpg)

## Rev 2
- Add "K" to  silkscreen for LEDs
- Add values to resistors
- Add load sensor pinout to to silkscreen

